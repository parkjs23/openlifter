// vim: set ts=2 sts=2 sw=2 et:
// @flow
//
// This file is part of OpenLifter, simple Powerlifting meet software.
// Copyright (C) 2019 The OpenPowerlifting Project.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Displays the selector for determining how many plates are available
// to loaders on one side.

import React from "react";
import { connect } from "react-redux";

import { FormControl, FormGroup, Table } from "react-bootstrap";

import { setPlatePairCount } from "../../actions/meetSetupActions";

import { kg2lbs } from "../../logic/units";

import type { PlatePairCount } from "../../types/dataTypes";
import type { GlobalState } from "../../types/stateTypes";

interface StateProps {
  inKg: boolean;
  platePairCounts: Array<PlatePairCount>;
}

interface DispatchProps {
  setPlatePairCount: (number, number) => any;
}

type Props = StateProps & DispatchProps;

class Plates extends React.Component<Props> {
  constructor(props, context) {
    super(props, context);

    this.validateAmountInput = this.validateAmountInput.bind(this);
    this.updateAmountHandler = this.updateAmountHandler.bind(this);
  }

  validateAmountInput = id => {
    const widget: any = document.getElementById(id);

    // This can happen because the FormGroup is created before the widget exists.
    if (widget === null) return;
    const value = widget.value;

    if (value === undefined) return "error";

    // Ensure that the value is an integer in a reasonable range.
    let asNum = Number(value);
    if (Math.floor(asNum) !== asNum) return "error";
    if (asNum < 0 || asNum > 20) return "error";
    if (String(asNum) !== value) return "error";

    return null;
  };

  updateAmountHandler = (weightKg, id) => {
    if (this.validateAmountInput(id) === "error") {
      // Although no state is set, this is used to trigger the FormGroup
      // to re-query the validationState on change.
      return this.setState({});
    }

    const widget: any = document.getElementById(id);
    this.props.setPlatePairCount(weightKg, Number(widget.value));
  };

  renderWeightRow = (weightKg, amount) => {
    // The input event value isn't passed by the event, so we assign a unique ID
    // and then just search the whole document for it.
    const id = "weight" + String(weightKg);

    // Don't use displayWeight(): the 1.25lb plates need two decimal places.
    const weight = this.props.inKg ? weightKg : kg2lbs(weightKg);

    return (
      <tr key={weightKg}>
        <td>{weight}</td>
        <td>
          <FormGroup validationState={this.validateAmountInput(id)} style={{ marginBottom: 0 }}>
            <FormControl
              id={id}
              onChange={e => this.updateAmountHandler(weightKg, id)}
              type="number"
              defaultValue={amount}
              min={0}
            />
          </FormGroup>
        </td>
      </tr>
    );
  };

  render() {
    let plateRows = [];
    for (let i = 0; i < this.props.platePairCounts.length; i++) {
      const obj: PlatePairCount = this.props.platePairCounts[i];
      plateRows.push(this.renderWeightRow(obj.weightKg, obj.pairCount));
    }

    const units = this.props.inKg ? "kg" : "lbs";

    return (
      <div>
        <Table striped condensed hover style={{ margin: "0px" }}>
          <thead>
            <tr>
              <th>Weight ({units})</th>
              <th>Pairs of Plates</th>
            </tr>
          </thead>
          <tbody>{plateRows}</tbody>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = (state: GlobalState): StateProps => ({
  inKg: state.meet.inKg,
  platePairCounts: state.meet.platePairCounts
});

const mapDispatchToProps = (dispatch): DispatchProps => {
  return {
    setPlatePairCount: (weightKg, amount) => dispatch(setPlatePairCount(weightKg, amount))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Plates);
